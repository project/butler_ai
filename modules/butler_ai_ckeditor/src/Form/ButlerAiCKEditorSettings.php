<?php

namespace Drupal\butler_ai_ckeditor\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\butler_ai\ButleraiOpenaiConnection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManager;

/**
 * Butler AI CKEditor configuration form.
 */
class ButlerAiCKEditorSettings extends ConfigFormBase {

  /**
   * Butler AI Open AI Connection object.
   *
   * @var \Drupal\butler_ai\ButleraiOpenaiConnection
   */
  protected $openaiConnection;

  /**
   * The entityTypeManager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Class constructor.
   */
  public function __construct(ButleraiOpenaiConnection $openaiConnection, EntityTypeManager $entityTypeManager) {
    $this->openaiConnection = $openaiConnection;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $container->get('butler_ai.open_ai.connection'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'butler_ai_ckeditor.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'butler_ai_ckeditor_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildForm($form, $form_state);
    $config = $this->config('butler_ai_ckeditor.settings');

    $form['butler_ckeditor'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('CKEditor contextual autocomplete'),
    ];

    $form['butler_ckeditor']['ckeditor_engine'] = [
      '#type' => 'select',
      '#title' => $this->t('OpenAI CKEditor engine'),
      '#description' => $this->t('OpenAI favourite engine to be used in API calls. This engine may impact on your API costs, see more information in openai.com.'),
      '#options' => $this->getEnginesList(),
      '#default_value' => $config->get('ckeditor_engine'),
    ];

    $form['butler_ckeditor']['ckeditor_tokens'] = [
      '#type' => 'number',
      '#title' => $this->t('Tokens'),
      '#description' => $this->t('OpenAI CKEditor tokens per call.'),
      '#maxlength' => 255,
      '#size' => 100,
      '#default_value' => $config->get('ckeditor_tokens'),
    ];

    $form['butler_ckeditor']['ckeditor_type'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Type'),
      '#description' => $this->t("OpenAI CKEditor call type. Don't change if you are not sure what you are doing."),
      '#maxlength' => 255,
      '#size' => 100,
      '#default_value' => $config->get('ckeditor_type'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('butler_ai_ckeditor.settings')
      ->set('ckeditor_engine', $form_state->getValue('ckeditor_engine'))
      ->set('ckeditor_tokens', $form_state->getValue('ckeditor_tokens'))
      ->set('ckeditor_type', $form_state->getValue('ckeditor_type'))
      ->save();
  }

  /**
   * {@inheritdoc}
   */
  private function getEnginesList(): array {
    $options = [];
    $options[] = $this->t('-- Any --');
    $terms = $this->entityTypeManager->getStorage('taxonomy_term')->loadByProperties([
      'vid' => 'butler_ai_open_ai_engines',
    ]);

    foreach ($terms as $term) {
      $options[$term->getName()] = $term->getName();
    }

    return $options;
  }

}
