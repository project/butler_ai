<?php

namespace Drupal\butler_ai_ckeditor\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "Open AI" plugin, with a CKEditor.
 *
 * @CKEditorPlugin(
 *   id = "openaiplugin",
 *   label = @Translation("OpenAI Plugin")
 * )
 */
class OpenAIPlugin extends CKEditorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getDependencies(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function isInternal() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return $this->getModulePath('butler_ai_ckeditor') . '/js/plugins/openai/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    $path = $this->getModulePath('butler_ai_ckeditor') . '/js/plugins/openai';
    return [
      'openaiplugin' => [
        'label' => $this->t('Attach Open AI content.'),
        'image' => $path . '/images/icon.png',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

}
