<?php

namespace Drupal\butler_ai_ckeditor\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\butler_ai\ButleraiOpenaiConnection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Butler AI CKEditor contextual autocomplete controller.
 */
class ButlerAiContextualAutocomplete extends ControllerBase {

  /**
   * Butler AI Open AI Connection object.
   *
   * @var \Drupal\butler_ai\ButleraiOpenaiConnection
   */
  protected $openaiConnection;

  /**
   * Class constructor.
   */
  public function __construct(ButleraiOpenaiConnection $openaiConnection) {
    $this->openaiConnection = $openaiConnection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $container->get('butler_ai.open_ai.connection')
    );
  }

  /**
   * Main controller content method that returns response values.
   */
  public function content(Request $request) {
    $config = $this->config('butler_ai_ckeditor.settings');

    $engine = $config->get('ckeditor_engine');
    $type = $config->get('ckeditor_type');
    $maxTokens = (int) $config->get('ckeditor_tokens');
    $prompt = $request->request->get('prompt');

    $options = [
      "headers" => [
        "Content-Type" => "application/json",
      ],
      "json" => [
        "prompt" => $prompt,
        "max_tokens" => $maxTokens,
        "model" => $engine,
      ],
    ];

    $response = $this->openaiConnection->getData($options, 'POST', $engine, $type);

    return new JsonResponse($response['choices'][0]['text']);
  }

}
