/**
 * @file
 * Butler Open AI plugin definition.
 */

(function ($, Drupal, drupalSettings, CKEDITOR) {

  CKEDITOR.config.contentsCss = '/css/ckeditor_overrides.css';
  CKEDITOR.plugins.add('openaiplugin', {
    init: function (editor) {
      'use strict';

      editor.addCommand('openai_template', {
        exec: function (editor) {
          var selectedHtml = "";
          var selection = editor.getSelection();
          if (selection) {
            selectedHtml = getSelectionHtml(selection);
          }
          $.post(
            drupalSettings.butlerCKEditorEndpoint,
            {prompt:selectedHtml},
            function (response) {
              editor.insertHtml(selectedHtml + response);
            }
          )
          .fail(function() {
            alert('The Butler AI CKEditor request has finished without success. Please, ensure your API key is valid and it is well configured on Butler AI settings form.');
          });
        }
      });

      editor.ui.addButton('openaiplugin', {
        label: 'OpenAI (WYSIWYG)',
        toolbar: 'insert',
        command: 'openai_template',
        icon: this.path + 'images/icon.png'
      });
    }
  });

  /**
   Get HTML of a selection.
   */
  function getSelectionHtml(selection) {
    var ranges = selection.getRanges();
    var html = '';
    for (var i = 0; i < ranges.length; i++) {
      var content = ranges[i].extractContents();
      html += content.getHtml();
    }
    return html;
  }

})(jQuery, Drupal, drupalSettings, CKEDITOR);
