/* eslint-disable import/no-extraneous-dependencies */
// cspell:ignore aipluginediting

import { Plugin } from 'ckeditor5/src/core';
import AiPluginEditing from './aipluginediting';

/**
 * Define a class that will retrieve Butler AI CKEditor functions.
 *
 * @private
 */
class AiPlugin extends Plugin {
  /**
   * @inheritdoc
   */
  static get requires() {
    return [AiPluginEditing];
  }

  /**
   * @inheritdoc
   */
  static get pluginName() {
    return 'AiPlugin';
  }
}

export default AiPlugin;
