/* eslint-disable import/no-extraneous-dependencies */
import { Plugin } from 'ckeditor5/src/core';
import { ButtonView } from 'ckeditor5/src/ui';
import icon from '../../../../icons/butler_ai_ckeditor.svg';

/**
 * Calls to the Butler AI autocomplete route.
 *
 * @private
 */
class AiPluginEditing extends Plugin {
  /**
   * @inheritdoc
   */
  static get pluginName() {
    return 'AiPluginEditing';
  }

  /**
   * @inheritdoc
   */
  init() {
    const editor = this.editor;

    editor.commands.add('butlerAiPlugin');

    editor.ui.componentFactory.add('butleraickeditorplugin', (locale) => {
      const command = editor.commands.get('butlerAiPlugin');
      const buttonView = new ButtonView(locale);

      // Create the toolbar button.
      buttonView.set({
        label: editor.t('Butler AI CKEditor5 plugin'),
        icon,
        tooltip: true,
      });

      // Execute the command when the button is clicked (executed).
      this.listenTo(buttonView, 'execute', () =>
        this.openai_template(editor),
      );

      return buttonView;
    });

  }

  openai_template(editor) {
    // Get editor selected text.
    var range = editor.model.document.selection.getFirstRange();
    for (const item of range.getItems()) {
      var selection = item.data;
    }

    // Create an API call.
    jQuery.post(
      drupalSettings.butlerCKEditorEndpoint,
      {prompt:selection},
      function (response) {
        // Insert the API response into editor.
        editor.model.change( writer => {
          const insertPosition = editor.model.document.selection.getLastPosition();
          writer.insertText(response, insertPosition);
      } );
      }
    )
    .fail(function() {
      alert('The Butler AI CKEditor request has finished without success. Please, ensure your API key is valid and it is well configured on Butler AI settings form.');
    });

  }

}

export default AiPluginEditing;
