<?php

namespace Drupal\butler_ai\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\butler_ai\ButleraiOpenaiConnection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Open AI connection configuration form.
 */
class ButlerOpenAiConnectionSettings extends ConfigFormBase {

  /**
   * Class constructor.
   */
  public function __construct(ButleraiOpenaiConnection $openaiConnection) {
    $this->openaiConnection = $openaiConnection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $container->get('butler_ai.open_ai.connection')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'butler_ai.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'butler_ai_connection_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('butler_ai.settings');

    $form['openai_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OpenAI url'),
      '#description' => $this->t('OpenAI API endpoint.'),
      '#maxlength' => 255,
      '#size' => 100,
      '#default_value' => $config->get('openai_url'),
    ];

    $form['openai_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OpenAI token'),
      '#description' => $this->t('OpenAI API personal token string.'),
      '#maxlength' => 255,
      '#size' => 100,
      '#default_value' => $config->get('openai_token'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('butler_ai.settings')
      ->set('openai_url', $form_state->getValue('openai_url'))
      ->set('openai_token', $form_state->getValue('openai_token'))
      ->save();

    // Refresh engines list whenever token has been updated.
    if ($form_state->getCompleteForm()['openai_token']['#default_value'] != $form_state->getCompleteForm()['openai_token']['#value']) {
      if ($this->openaiConnection->updateEnginesList()) {
        $this->messenger()->addStatus('Engines list has been updated with success!');
      }
    }
  }

}
