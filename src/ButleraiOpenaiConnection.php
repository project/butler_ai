<?php

namespace Drupal\butler_ai;

use GuzzleHttp\ClientInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Component\Serialization\Json;
use GuzzleHttp\Psr7\Response;
use Drupal\taxonomy\Entity\Term;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Messenger\Messenger;

/**
 * Butler AI Open AI connection class.
 */
class ButleraiOpenaiConnection implements ButleraiConnectionInterface {

  /**
   * GuzzleHttp\ClientInterface definition.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Drupal\Core\Config\ConfigFactory definition.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $config;

  /**
   * Constructs a new ButleraiOpenaiConnection object.
   */
  public function __construct(
    ClientInterface $http_client,
    ConfigFactory $config,
    EntityTypeManager $entityTypeManager,
    Messenger $messenger
    ) {
    $this->httpClient = $http_client;
    $this->config = $config->get('butler_ai.settings');
    $this->entityTypeManager = $entityTypeManager;
    $this->messenger = $messenger;
  }

  /**
   * Connection method.
   *
   * @return mixed
   *   Connection result.
   */
  protected function connection($options = [], $method = 'GET', $engine = 'engines', $type = NULL): Response {

    $url = $this->config->get('openai_url');
    $url = $type ? $url . $type : $url . $engine;

    $apiToken = $this->config->get('openai_token');
    $options['headers']['Authorization'] = "Bearer $apiToken";

    return $this->httpClient->request($method, $url, $options);
  }

  /**
   * Check connection public method.
   *
   * @return bool
   *   Connection bool status.
   */
  public function checkConnection(): bool {
    return $this->connection()->getStatusCode() == 200 ? TRUE : FALSE;
  }

  /**
   * Get favourite type stored in module's configuration.
   *
   * @return string
   *   Favourite type retrieved from configuration settings.
   */
  protected function getType(): ?string {
    return $this->config->get('openai_type');
  }

  /**
   * Create or update engines list.
   */
  public function updateEnginesList() {
    try {
      $engines = Json::decode($this->connection()->getBody()->getContents());
      if (isset($engines['data'])) {
        $entity_type = 'taxonomy_term';
        $vid = 'butler_ai_open_ai_engines';

        $tids = $this->entityTypeManager->getStorage($entity_type)->loadByProperties([
          'vid' => $vid,
        ]);
        $this->entityTypeManager->getStorage($entity_type)->delete($tids);

        foreach ($engines['data'] as $engine) {
          if ('engine' === $engine['object']) {
            $values = [
              'vid' => $vid,
              'name' => $engine['id'],
            ];
            Term::create($values)->save();
          }
        }
      }
    }
    catch (\Exception $e) {
      $this->messenger->addError('The API key inserted is not a valid key. Please, find more information in Open AI official website.');
      return FALSE;
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getData($options = [], $method = 'GET', $engine = NULL, $type = NULL): array {

    try {
      $response = $this->connection($options, $method, $engine, $type);
    }
    catch (\Exception $e) {
      throw new \Exception('OpenAI request return an error.');
    }

    return Json::decode($response->getBody()->getContents()) ?? [];
  }

}
