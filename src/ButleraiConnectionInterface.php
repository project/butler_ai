<?php

namespace Drupal\butler_ai;

/**
 * Butler AI connection interface.
 */
interface ButleraiConnectionInterface {

  /**
   * {@inheritdoc}
   */
  public function checkConnection(): bool;

  /**
   * {@inheritdoc}
   */
  public function getData(): array;

}
