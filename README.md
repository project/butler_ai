# Butler AI
Butler AI is a module to help editorial team members including next generation
artificial intelligence and machine learning external services such as Open AI.

Editors will be able to create content in a different and easier way, saving
time and being more efficient.

## Document contents
### [Installation]
### [Requirements]
### [Configuration]
### [License]

***

[Installation]: #Installation
## Installation
If you are using composer, write following command:

```bash
composer require drupal/butler_ai
```

[Requirements]: #Requirements
## Requirements
This module requires no modules outside of Drupal core.

[Configuration]: #Configuration
## Configuration
No configuration is needed.

[License]: #License
## License
GNU General Public License, version 2
